package pl.news.observer;

import pl.news.WeatherForecast;

public class RadioNews implements Observer{
    @Override
    public void updateForecast(WeatherForecast weatherForecast) {
        System.out.println("RadioNews - pogoda się zmieniła: "+weatherForecast.getPressure() + " "+weatherForecast.getTemperature());
    }
}

package pl.news.observer;

import pl.news.WeatherForecast;

public class TvNews implements Observer{
    @Override
    public void updateForecast(WeatherForecast weatherForecast) {
            System.out.println("TvNews - pogoda się zmieniła: "+weatherForecast.getPressure() + " "+weatherForecast.getTemperature());
    }
}

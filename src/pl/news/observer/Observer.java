package pl.news.observer;

import pl.news.WeatherForecast;

public interface Observer {
    void updateForecast(WeatherForecast weatherForecast);
}

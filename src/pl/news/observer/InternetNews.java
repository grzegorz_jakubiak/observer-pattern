package pl.news.observer;

import pl.news.WeatherForecast;

public class InternetNews implements Observer{
    @Override
    public void updateForecast(WeatherForecast weatherForecast) {
        System.out.println("InternetNews - pogoda się zmieniła: "+weatherForecast.getPressure() + " "+weatherForecast.getTemperature());
    }
}
